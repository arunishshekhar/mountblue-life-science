# SOA Service-Oriented Architecture

## What is it?

**SOA or service-oriented architecture** is an approach in which the development takes place over distributed services. In simple words, it is a way in which a **Big Work** is broken into smaller **tasks** and then are merged once completed.

## How it works...   

![SOA Components](SOA_A.png)
- **Client**
    : A client is someone who requests the usage or access of the service of the System. It can be accessed by _internal services_ or an _external service_.
- **API Gateway**
    : It is an optional past that is provided for security.
- **Service**
    : These are the individual components (asset, resource, blob, etc) that are accessed by the client.
- **ESB or Enterprise Service Bus**
    : It is like the middleman that handles the integration of services, routing, providing connectivity, monitoring and logging.
- **SDR or Service Description Repository/Registry** 
    : They communicate with ESB to provide information about the SOA system.

## Benefits...
- **Service reusability** 
    : Applications are made from existing services. Thus, services can be reused to make many applications.
- **Easy maintenance**
    : As services are independent of each other they can be updated and modified easily without affecting other services.
- **Platform independent**
    : SOA allows making a complex application by combining services picked from different sources, independent of the platform.
- **Reliability**
    : SOA applications are more reliable because it is easy to debug small services rather than huge codes
- **Scalability**
    : Services can run on different servers within an environment, this increases scalability.

## Disadvantage...
- **High overhead**
    : A validation of input parameters of services is done whenever services interact this decreases performance as it increases load and response time.
- **High investment**
    : A huge initial investment is required for SOA.
- **Complex service management**
    : When services interact they exchange messages to tasks. The number of messages may go in millions. It becomes a painful task to handle a large number of messages.

## How will it help us...
- ***Performance***
    - Since, the workload is distributed it improves the performance.
    - When code becomes reliable it becomes better improving the performance.
    
- ***Scaling***
    - Since it is platform-independent scaling is easier.
    - Scaling is literary one of its benefits.
    - It is easy to maintain.

## Implementation Examples
- Delaware Electric turned to SOA to integrate systems that previously did not talk to each other, resulting in development efficiencies that helped the organization stay solvent during a five-year, state-mandated freeze on electric rates.
- Cisco adopted SOA to make sure its product ordering experience was consistent across all products and channels by exposing ordering processes as services that Cisco’s divisions, acquisitions, and business partners could incorporate into their websites.
- Independence Blue Cross (IBC) of Philadelphia implemented an SOA to ensure that the different constituents dealing with patient data—IBC customer service agents, physicians’ offices, IBC website users—were working with the same data source (a ‘single version of the truth’).
## Resources and References
- [Wikipedia](https://en.wikipedia.org/wiki/Service-oriented_architecture)
- [GeeksForGeeks](https://www.geeksforgeeks.org/service-oriented-architecture/)
- [IBM](https://www.ibm.com/in-en/cloud/learn/soa#toc-what-is-an-sqoa7ntn)
- [YouTube](https://www.youtube.com/watch?v=jNiEMmoTDoE)